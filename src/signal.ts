import copy from 'fast-copy';
import { deepEqual } from 'fast-equals';
import { ISignalOptions } from './i-signal-options';

export type Effect = () => void;

export class Signal<T> {
    private _value: T;
    private _observers: Effect[] = [];
    private _options: ISignalOptions;

    public constructor(value: T, options: ISignalOptions = null) {
        this._value = value;
        this._options = options ?? {
            deepEquality: true
        };
    }

    public get value(): T {
        return copy(this._value);
    }

    public set value(value: T) {
        let canSetValue = false;
        try {
            if (this._options?.equalityFn != null) {
                canSetValue = !this._options.equalityFn(this._value, value);
            } else if (this._options?.deepEquality != true) {
                // shallow compare... obviously doesn't do anything for objects/arrays/maps etc
                canSetValue = !(this._value == value);
            } else {
                canSetValue = !deepEqual(this._value, value);
            }
        } catch (ex) {
            console.error(ex);
        }

        if (canSetValue == true) {
            this._value = copy(value);
            for (let obs of this._observers) {
                try {
                    obs();
                } catch (ex) {
                    console.error(ex);
                }
            }
        }
    }

    /**
     * WARNING: USE ONLY FOR VIEW ONLY PURPOSES. DO NOT USE THIS TO SET THE VALUE OR PROPERTIES ON THE VALUE.
     * @returns a direct reference to the underlying value. Only use this for display only purposes.
     */
    public get peek(): T {
        return this._value;
    }

    public subscribe(callback: Effect) {
        if (this._observers.indexOf(callback) == -1) {
            this._observers.push(callback);
        }
        return callback; // return a ref so the call can unsubscribe
    }

    public unsubscribe(callback: Effect) {
        const index = this._observers.indexOf(callback);
        if (index > -1) {
            this._observers.splice(index, 1);
        }
    }
}