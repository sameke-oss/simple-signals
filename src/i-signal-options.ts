export type Predicate<T> = (a: T, b: T) => boolean;

export interface ISignalOptions {
    deepEquality?: boolean;
    equalityFn?: Predicate<any>;
}