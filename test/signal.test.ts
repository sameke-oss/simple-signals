import { describe } from 'mocha';
import { expect } from 'chai';
import { Signal } from '../src';

describe('signal', () => {
    it('should get value', () => {
        let count = new Signal(0);
        expect(count.value).to.equal(0);
        let count2 = new Signal(1);
        expect(count2.value).to.equal(1);
    });

    it('should set value', () => {
        let count = new Signal(0);
        count.value = 1;
        expect(count.value).to.equal(1);
        count.value++;
        expect(count.value).to.equal(2);
    });

    it('should notify listeners', (done) => {
        let count = new Signal(0);
        let hasExited = false;
        let timer = setTimeout(() => {
            expect(false).to.be.true;
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        }, 100);
        count.subscribe(() => {
            clearTimeout(timer);
            expect(count.value).to.equal(5);
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        });
        count.value = 5;
    });

    it('should perform deep equality check by default', (done) => {
        let a = {
            foo: 'bar'
        };

        let b = {
            foo: 'bar'
        };

        // if we do not get callback then we know equality check failed
        let myVar = new Signal(a);
        let hasExited = false;
        let timer = setTimeout(() => {
            // we should hit here because deep equality check should find the two objects equal
            expect(true).to.be.true;
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        }, 100);

        myVar.subscribe(() => {
            clearTimeout(timer);
            expect(false).to.be.true;
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        });

        myVar.value = b;
    });

    it('should not perform deep equality check', (done) => {
        let a = {
            foo: 'bar'
        };

        let b = {
            foo: 'bar'
        };

        // if we do not get callback then we know equality check failed
        let myVar = new Signal(a, { deepEquality: false });
        let hasExited = false;
        let timer = setTimeout(() => {
            // we should hit here because deep equality check should find the two objects equal
            expect(false).to.be.true;
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        }, 100);

        myVar.subscribe(() => {
            clearTimeout(timer);
            expect(true).to.be.true;
            if (hasExited != true) {
                done();
            }
            hasExited = true;
        });

        myVar.value = b;
    });
});