# simple-signals

> A very simple reactive signals implementation for use with your favorite web framework

- Intuitive
- Immutable
- Very Light Weight

simple-signals will work with any framework

## Installation

**add the registry to your projects .npmrc file**
1. `@sameke-oss:registry=https://gitlab.com/api/v4/projects/53081603/packages/npm/`
2. `npm install @sameke-oss/simple-signals`

# Usage

```typescript
import { Signal } from '@sameke-oss/simple-signals';

let nameSignal = new Signal('Ted');
let subscription = nameSignal.subscribe(() => {
    console.log(`name changed to ${nameSignal.value}.`);
});

nameSignal.value = 'Mary';

// WARNING: DON'T FORGET TO UNSUBSCRIBE FROM SIGNALS ON PAGE NAVIGATION
nameSignal.unsubscribe(subscription);
```